
# Employee Details Management System

## Overview

This project is an Employee Details Management System, which includes a form for entering employee details and an admin page for viewing the existing employees. The application is built using Flask for the backend and Tailwind CSS for styling, with Alpine.js for front-end interactivity and validation.

## Features

- **Employee Details Form**: A responsive form for entering employee details with validation and helper text.
- **Admin Page**: A responsive table displaying a list of existing employees.
- **PDF Generation**: Option to generate a PDF of the employee list.

## Technologies Used

- **Backend**: Flask (Python)
- **Frontend**: Tailwind CSS, Alpine.js
- **Templating**: Jinja2 (used with Flask)

## Installation

1. **Clone the repository**:

   ```bash
   git clone https://github.com/your-username/employee-details-management.git
   cd employee-details-management
   ```
2. **Create a virtual environment and activate it**:

   ```bash
   python -m venv venv
   source venv/bin/activate  # On Windows: venv\Scripts\activate
   ```
3. **Install the required packages**:

   ```bash
   pip install -r requirements.txt
   ```
4. **Run the application**:

   ```bash
   flask run
   ```
5. **Open your browser and navigate to**:

   ```
   http://127.0.0.1:5000
   ```

## Project Structure

```
employee-details-management/
├── static/
│   └── css/
│       └── tailwind.css
├── templates/
│   ├── form.html
│   ├── admin.html
│   └── base.html
├── app.py
├── requirements.txt
└── README.md
```

- `static/css/tailwind.css`: Tailwind CSS file.
- `templates/`: Contains the HTML templates.
  - `form.html`: Template for the employee details form.
  - `admin.html`: Template for the admin page displaying existing employees.
  - `base.html`: Base template with common HTML structure.
- `app.py`: Main Flask application file.
- `requirements.txt`: List of Python packages required for the project.
- `README.md`: This README file.

## Usage

### Employee Details Form

1. Navigate to the home page to access the employee details form.
2. Fill out the form with the employee information.
3. Submit the form. The form includes validation for the mobile number (10 digits) and Aadhaar number (12 digits).

### Admin Page

1. Navigate to `/admin` to view the list of existing employees.
2. The table is responsive and displays all the employee details.
3. Click on "Generate PDF" to create a PDF of the employee list.

## Tailwind CSS and Alpine.js

- **Tailwind CSS**: A utility-first CSS framework for rapid UI development.
- **Alpine.js**: A minimal framework for composing JavaScript behavior in your HTML.

### Customization

- Tailwind CSS classes are used throughout the templates for styling. You can customize the styles by modifying the Tailwind configuration or updating the classes in the HTML files.
- Alpine.js is used for form validation. You can extend or modify the validation logic in the `x-data` attributes in the HTML.

## Contributing

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Open a Pull Request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

---

Feel free to customize the README further based on your specific project details and requirements.
