from flask import Flask, render_template, redirect, url_for, send_file, current_app
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from flask_migrate import Migrate
from weasyprint import HTML
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = 'your_secret_key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///employees.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    fathers_name = db.Column(db.String(100), nullable=False)
    mothers_name = db.Column(db.String(100), nullable=False)
    spouse_name = db.Column(db.String(100), nullable=True)
    type_of_employment = db.Column(db.String(50), nullable=False)
    date_of_birth = db.Column(db.String(20), nullable=False)
    mobile_no = db.Column(db.String(20), nullable=False)
    pan_no = db.Column(db.String(20), nullable=False)
    adhaar_no = db.Column(db.String(20), nullable=False)
    pay_id = db.Column(db.String(20), nullable=False)
    ifsc_code = db.Column(db.String(20), nullable=False)
    bank_account_no = db.Column(db.String(20), nullable=False)
    pvc_done = db.Column(db.String(5), nullable=False)
    pvc_reference_no = db.Column(db.String(50), nullable=False)

class EmployeeForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    fathers_name = StringField('Father\'s Name', validators=[DataRequired()])
    mothers_name = StringField('Mother\'s Name', validators=[DataRequired()])
    spouse_name = StringField('Spouse Name')
    type_of_employment = StringField('Type of Employment', validators=[DataRequired()])
    date_of_birth = StringField('Date of Birth', validators=[DataRequired()])
    mobile_no = StringField('Mobile No', validators=[DataRequired()])
    pan_no = StringField('PAN No', validators=[DataRequired()])
    adhaar_no = StringField('Adhaar No', validators=[DataRequired()])
    pay_id = StringField('Pay ID', validators=[DataRequired()])
    ifsc_code = StringField('IFSC Code', validators=[DataRequired()])
    bank_account_no = StringField('Bank Account No', validators=[DataRequired()])
    pvc_done = StringField('PVC Done', validators=[DataRequired()])
    pvc_reference_no = StringField('PVC Reference No', validators=[DataRequired()])
    submit = SubmitField('Add Employee')

@app.route('/', methods=['GET', 'POST'])
def index():
    form = EmployeeForm()
    if form.validate_on_submit():
        new_employee = Employee(
            name=form.name.data,
            fathers_name=form.fathers_name.data,
            mothers_name=form.mothers_name.data,
            spouse_name=form.spouse_name.data,
            type_of_employment=form.type_of_employment.data,
            date_of_birth=form.date_of_birth.data,
            mobile_no=form.mobile_no.data,
            pan_no=form.pan_no.data,
            adhaar_no=form.adhaar_no.data,
            pay_id=form.pay_id.data,
            ifsc_code=form.ifsc_code.data,
            bank_account_no=form.bank_account_no.data,
            pvc_done=form.pvc_done.data,
            pvc_reference_no=form.pvc_reference_no.data
        )
        db.session.add(new_employee)
        db.session.commit()
        return redirect(url_for('index'))
    employees = Employee.query.all()
    return render_template('form.html', form=form, employees=employees)


@app.route('/admin')
def admin():
    employees = Employee.query.all()
    return render_template('admin.html', employees=employees)

@app.route('/generate_pdf')
def generate_pdf():
    try:
        employees = Employee.query.all()
        html_content = render_template('template.html', employees=employees)
        pdf_file = '/tmp/employee_details.pdf'
        HTML(string=html_content).write_pdf(pdf_file)
        return send_file(pdf_file, as_attachment=True)
    except Exception as e:
        return f"Failed to generate PDF: {str(e)}", 500

if __name__ == '__main__':
    with app.app_context():
        db.create_all()
    app.run(debug=True)
